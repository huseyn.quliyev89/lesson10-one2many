package az.ingress.one2many.repository;

import az.ingress.one2many.model.Phone;
import az.ingress.one2many.model.PhoneUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone,Integer> {
}
