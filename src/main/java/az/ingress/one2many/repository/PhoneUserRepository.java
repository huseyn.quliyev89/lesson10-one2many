package az.ingress.one2many.repository;

import az.ingress.one2many.model.PhoneUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneUserRepository extends JpaRepository<PhoneUser,Integer> {
}
