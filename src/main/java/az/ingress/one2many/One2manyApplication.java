package az.ingress.one2many;

import az.ingress.one2many.model.Phone;
import az.ingress.one2many.model.PhoneUser;
import az.ingress.one2many.repository.PhoneRepository;
import az.ingress.one2many.repository.PhoneUserRepository;
import org.hibernate.query.sqm.mutation.internal.temptable.LocalTemporaryTableInsertStrategy;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
public class One2manyApplication implements CommandLineRunner {

private final PhoneUserRepository phoneUserRepository;
private final PhoneRepository phoneRepository;

	public One2manyApplication(PhoneUserRepository phoneUserRepository, PhoneRepository phoneRepository) {
		this.phoneUserRepository = phoneUserRepository;
		this.phoneRepository = phoneRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(One2manyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// --Unidirectional--
//		Phone phone1 = Phone.builder()
//				.type("mobile")
//				.no("+994512296643")
//				.build();
//
//		Phone phone2 = Phone.builder()
//				.type("mobile")
//				.no("+994552296643")
//				.build();
//
//		PhoneUser phoneUser = PhoneUser.builder()
//				.name("Huseyn")
//				.surname("Guliyev")
//				.phoneList(List.of(phone1,phone2))
//				.build();
//
//		phoneUserRepository.save(phoneUser);
		// --Unidirectional--

		// --Bidirectional--
		Phone phone1 = Phone.builder()
				.type("mobile")
				.no("+994512296643")
				.build();

		Phone phone2 = Phone.builder()
				.type("mobile")
				.no("+994552296643")
				.build();

		PhoneUser phoneUser = PhoneUser.builder()
				.name("Huseyn")
				.surname("Guliyev")
				.phoneList(List.of(phone1,phone2))
				.build();

		phone1.setPhoneUser(phoneUser);
		phone2.setPhoneUser(phoneUser);
     	phoneUserRepository.save(phoneUser);

		 Phone phone3 = phoneRepository.findById(1).get();
		 System.out.println(phone3);
		 System.out.println(phone3.getPhoneUser());

		 PhoneUser phoneUser2 = phoneUserRepository.findById(1).get();
		 System.out.println(phoneUser2);
		// --Bidirectional--


	}

}


