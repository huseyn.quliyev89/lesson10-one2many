package az.ingress.one2many.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "phone")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "type")
    private String type;

    @Column(name = "no")
    private String no;

 //   Bidirectional
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "phone_user_id", referencedColumnName = "id")
    @ToString.Exclude
    private PhoneUser phoneUser;

}
