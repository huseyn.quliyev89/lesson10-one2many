package az.ingress.one2many.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "phone_user")
public class PhoneUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    //Unidirectional
//    @OneToMany(cascade=CascadeType.ALL)
//    @JoinColumn(name = "phone_user_id", referencedColumnName = "id")
//    private List<Phone> phoneList;

    //Bidirectional
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "phoneUser", cascade = CascadeType.ALL)
    private List<Phone> phoneList;
}
